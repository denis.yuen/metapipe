#!/usr/bin/env nextflow

nextflow.preview.dsl = 2

/*
 * Default pipeline parameters. They can be overridden on the command line eg.
 * given `params.foo` specify on the run command line `--foo some_value`.
 */

//Help
params.help = false

//General
params.refdbDir = "${baseDir}/refdb"
params.export = "outputs"

//Inputs
params.reads = ""
params.contigs = ""

//Pipeline settings
params.trimSettings = 'AVGQUAL:20 SLIDINGWINDOW:4:15 MINLEN:75'
params.contigsCutoff = 1000
params.noBinning = false
params.noTaxo= false
params.kaiju_refdb = 'kaiju-mardb:1.7.2'
params.mapseq_refdb = 'silvamar:4'
params.noFunc = false
params.removeIncompleteGenes = false
params.diamond_refdb = 'diamond-marref-proteins:4'
params.diamond_sensitivity = 'sensitive'
params.interpro_refdb = 'interpro:5.42-78.0'
params.priam_refdb = 'priam:JAN18'

if (params.help)
    log.info """
M E T A P I P E
===================================

Usage: 'nextflow run http://gitlab.com/uit-sfb/metapipe [-r <x.y.z> | -latest] [-nextflow-options ...] [--reads "/path/to/{r1,r2}.fastq" | --contigs "/path/to/fasta"] [--metapipe-settings ...]

Common nextflow options:
  -r                                        Specify which version of metapipe to run (the list of releases can be found here: https://gitlab.com/uit-sfb/metapipe/-/releases).
                                            A specific (full) git hash from branch 'master' can alternatively be used.
  -latest                                   Pull the latest commit of branch 'master'. Alternative to '-r'.
                                            May not work if '-r' has been use in the past (deleting '.nextflow' directory fixes this issue).
  -resume                                   Use cached results and only reprocess tasks that previously failed or whose parameters changed in some way.
  -params-file <path/to/param.yml|.json>    Use parameters stored as key-value pairs in a Json/Yaml file.
                                            Remember to remove the '--' on the key side.
  -c <path/to/configFile>                   Provide config file (overlayed on top of the default one).
  -with-trace <trace.tsv>                   Generate a tsv file 'trace.tsv'
  -N <email_address>                        Send an email when workflow execution ends
  Find more nextflow options by running 'nextflow run -h'

Metapipe settings:
  General:
    --help                                      Display help message
    --export <path/to/export>                   Path to location where outputs are exported
                                                Default: "./outputs"
    --refdbDir <path/refdb/dir>                 Path to reference databases alocation
                                                Default: "${baseDir}/refdb"

  Inputs:
    There are several entrypoints to the workflow (or pathways). Pathways are mutially exclusive and only one can be used at a time.
    Illumina reads (FASTQ):
      --reads <path/to/{r1,r2}.fastq(.gz)>      Path to input Illumina reads FASTQ files (forward and reverse). May be gzipped.
                                                Ex: if the files are '/data/H707_fw.fastq.gz' and (/data/H707_rv.fastq.gz), depending on what other files are present in '/data'
                                                    then the parameter could be '--reads /data/H707_{fw, rv}.fastq.gz',
                                                    or '--reads /data/*_{fw, rv}.fastq.gz', or even '--reads /data/*{fw, rv}*'
      --reads <path/to/interleaved.fastq(.gz)>  Path to interleaved Illumina reads FASTQ file. May be gzipped.
    Contigs (FASTA):
      --contigs <path/to/contigs.fasta(.gz)>    Path to input contigs FASTA file. May be gzipped.
      Note: in this pathway only the Functional assignment stage is executed.

  Workflow settings:
    Assembly:
      --trimSettings                            Parameters for trimmomatic (both SE and PE)
                                                Default: 'AVGQUAL:20 SLIDINGWINDOW:4:15 MINLEN:75'
      --contigsCutoff                           Contigs length cutoff (any contig small than the cutoff are discarded)
                                                Default: 1000
    Binning:
      --noBinning                               Disable Binning stage
                                                Default: false
    Taxonomic classification:
      --noTaxo                                  Disable Taxonomic classification stage
                                                Default: false
      --kaiju_refdb                             Reference database used by Kaiju
                                                One of:
                                                  'kaiju-mardb:1.7.2' (default)
                                                  'kaiju-refseq:1.7.2'
                                                  '' (disables Kaiju)
      --mapseq_refdb                            Reference database used by MapSeq
                                                One of:
                                                  'silvamar:4' (default)
                                                  '' (disables MapSeq)
    Functional assignment:
      --noFunc                                  Disable Functional assignment stage
                                                Default: false
      --contigsCutoff                           Contigs length cutoff (any contig small than the cutoff are discarded)
                                                Default: 1000
      --removeIncompleteGenes                   Discard any incomplete gene
                                                Default: false
      --diamond_refdb                           Reference database used by Diamond
                                                One of:
                                                  'diamond-marref-proteins:4' (default)
                                                  'diamond-uniref50:2019-06'
                                                  '' (disables Diamond)
      --diamond_sensitivity                     Sensitivity setting for Diamond
                                                One of:
                                                  'sensitive' (default)
                                                  'more-sensitive'
      --interpro_refdb                          Reference database used by Interproscan
                                                One of:
                                                  'interpro:5.42-78.0' (default)
                                                  '' (disables Interproscan)
      --priam_refdb                             Reference database for Priamsearch
                                                One of:
                                                  'priam:JAN18'
                                                  '' (disables Priamsearch)
"""

include {Metapipe} from './nextflow/metapipe.nf'

workflow {
  if (!params.help) {
    //Config checks
      if (!params.reads && !params.contigs) {
        log.warn("No input. Please run with '--help' flag to display the list of available parameters.")
        exit(1)
      }

      if (params.reads && params.contigs) {
        log.error("Input may be only one of '--reads' or '--contigs'.")
        exit(1)
      }

      log.info """
M E T A P I P E
===================================
General:
refdbDir: ${params.refdbDir}
export: ${params.export}

Inputs:
  ${if(params.reads){"reads: ${params.reads}"} else {"contigs: ${params.contigs}"}}

Workflow settings:
${if(params.reads){
"""  Assembly:
     trimSettings: ${params.trimSettings}
     contigsCutoff: ${params.contigsCutoff}"""
 } else {
"  Assembly disabled (make sure to use input --reads)"
}}
${if(!params.noBinning && params.reads){
"""  Binning:
     enabled"""
  } else {
"  Binning disabled (to activate, remove --noBinning parameter or make sure to use input --reads)"
 }}
${if(!params.noTaxo && params.reads){
"""  Taxonomic classification:
     ${if(params.kaiju_refdb != true) "kaiju_refdb: ${params.kaiju_refdb}" else "Kaiju disabled. Set --kaiju_refdb to enable the tool."}
     ${if(params.mapseq_refdb != true) "mapseq_refdb: ${params.mapseq_refdb}" else "MapSeq disabled. Set --mapseq_refdb to enable the tool."}"""
  } else {
"  Taxonomic classification disabled (to activate, remove --noTaxo parameter or make sure to use input --reads)"
 }}
${if(!params.noFunc){
"""  Functional assignment:
     contigsCutoff: ${params.contigsCutoff}
     removeIncompleteGenes: ${params.removeIncompleteGenes}
     ${if(params.diamond_refdb != true){"""diamond_refdb: ${params.diamond_refdb}
     diamond_sensitivity: ${params.diamond_sensitivity}"""} else {"Diamond disabled. Set --diamond_refdb to enable the tool."}}
     ${if(params.interpro_refdb != true){"interpro_refdb: ${params.interpro_refdb}"} else {"Interproscan disabled. Set --interpro_refdb to enable the tool."}}
     ${if(params.priam_refdb != true){"priam_refdb: ${params.priam_refdb}"} else {"PriamSearch disabled. Set --priam_refdb to enable the tool."}}"""
   } else {
"  Functional assignment disabled (to activate, remove --noFunc parameter)"
  }}
"""

    Metapipe()
  }
}

 /*
  * completion handler
  */
workflow.onComplete {
  if (!params.help)
    workflow.success ? log.info("\nDone") : log.warn("\nOops .. something went wrong")
}
