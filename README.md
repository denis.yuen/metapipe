# Metapipe

[Metapipe](https://gitlab.com/uit-sfb/metapipe) is a workflow for analysis and annotation of metagenomic samples,
providing insight into phylogenetic diversity, as well as metabolic and functional properties of environmental communities.

As illustrated in the diagram below, the workflow is divided into four modules:
  - filtering and assembly
  - taxonomic classification
  - functional assignment
  - binning

![Metapipe workflow](source/images/schematics/agnostic_metapipe_bioinf_pipeline.png)  
*Metapipe workflow schematic*

The workflow was [designed](https://munin.uit.no/handle/10037/11180) to leverage the most modern tools to 
ensure both quality outputs and efficient resource usage.

In addition, it is now clear that the quality of the reference databases plays a role at least as important
as the tools themselves when it comes to output quality and execution speed.
This is why Metapipe makes use of *plugged-in* reference databases.

## Getting started

### Requirements

- [Nextflow](https://www.nextflow.io/docs/latest/getstarted.html#installation) ([requirements](https://www.nextflow.io/docs/latest/getstarted.html#requirements))
- [Docker](https://docs.docker.com/get-docker/)

### Executing the workflow

The command to start a Metapipe run is:
```bash
nextflow run http://gitlab.com/uit-sfb/metapipe [-r <x.y.z>] [--workflow-settings ...]
```

where `x.y.z` is the latest version found [here](https://gitlab.com/uit-sfb/metapipe/-/releases).

To display the list of available parameters, run: `nextflow run http://gitlab.com/uit-sfb/metapipe [-r <x.y.z>] --help`.

## Configuration

The workflow is provided with a default configuration which is sufficient to run Metapipe locally with the input files located in the [test](test) folder.
To be able to run Metapipe with larger datasets locally, or to run Metapipe on other executors (SLURM, Kubernetes, ...), a configuration file can optionally be provided
the `-c path/to/configFile` option to `nextflow run`.

Assuming Metapipe is to be executed on the machine where `nextflow run` is being run (local executor), the configuration file may look like this:
```
executor {
  cpus = 8
  memory = '48 GB'
  //queueSize = 1
}

process {
  memory = '10 GB'
  withLabel: 'assembly' {
    cpus = 4
  }
  withName: 'Megahit' {
    cpus = 8
  }
}
```

### Executor

The `executor` section specifies properties of the execution environment such as:
- `cpus`: maximum number of CPUs available to run tasks (default: unlimited)
- `memory`: maximum amount of memory available to run tasks (default: unlimited)
- `queueSize`: maximum number of simultaneous tasks (default: unlimited)
- other properties can be found [here](https://www.nextflow.io/docs/latest/config.html#scope-executor).

Note: when using local executor, in case of warning: `WARNING: Your kernel does not support swap limit capabilities or the cgroup is not mounted. Memory limited without swap.`,
the limits will not be respected and may result in slow execution due to too many tasks attempting to run in parallel.
Please follow [these instructions](https://www.serverlab.ca/tutorials/containers/docker/how-to-limit-memory-and-cpu-for-docker-containers/).

### Process

The `process` section let us adjust some task-level properties such as:
- `cpus`: maximum number of CPUs available to run **one** task (default: 1)
- `memory`: maximum number of CPUs available to run **one** task (default: '1 GB')
- other properties can be found [here](https://www.nextflow.io/docs/latest/process.html#directives).

In the example configuration given above:
- any instance of the Megahit task will run with 8 CPUs and 10 GB memory
- any other task tagged with `assembly` will run with 4 CPUs and 10 GB memory
- any other task will run with 1 CPUs and 10 GB memory

### Advanced configuration

Please see [here](https://www.nextflow.io/docs/latest/config.html).

## Troubleshooting

### Permission issue with Docker

If you cannot successfully execute `docker run hello-world` due to permission errors to access the Docker socket, you can either:
  - follow [this step](https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user)
  - or run `sudo nextflow run` instead of `nextflow run`

### Task killed (due to OOM)

If a process attempts to use more memory than it is allowed to, the process will be killed.
To allocate more memory to a process, set a suitable value for the field `memory` in the `process` section of the configuration file (`-c option`).

### java.io.FileNotFoundException: Too many open files 

You need to increase the nofile ulimit. For that, add the following line in your configuration file (`-c option`) in the `process` section:  
`containerOptions = '--ulimit nofile=10000:10000'`