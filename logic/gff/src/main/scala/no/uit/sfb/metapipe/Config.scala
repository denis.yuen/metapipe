package no.uit.sfb.metapipe

import java.nio.file.Path

case class Config(
    cpu: Int = 1,
    outDir: Path = null,
    fastaDir: Path = null,
    mgaOut: Path = null,
    priamOut: Path = null,
    diamondOut: Path = null,
    interproOut: Path = null
)
