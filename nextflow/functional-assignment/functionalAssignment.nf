include {PreProcessContigs} from './process/preProcessContigs.nf' params(contigsCutoff: params.contigsCutoff)
include {SplitCds} from './process/splitCds.nf'
include {Mga} from './process/mga.nf'
include {Gff} from './process/gff.nf'
include {GeneExtractor} from './process/geneExtractor.nf' params(removeIncompleteGenes: params.removeIncompleteGenes)
include {Priam} from './priam.nf' params(refdb: params.priam_refdb, refdbDir: params.refdbDir)
include {Diamond} from './diamond.nf' params(refdb: params.diamond_refdb, sensitivity: params.diamond_sensitivity, refdbDir: params.refdbDir)
include {Interproscan} from './interproscan.nf' params(refdb: params.interpro_refdb, refdbDir: params.refdbDir)
include {Export} from '../helper/export.nf'

workflow FunctionalAssignment {
  take:
    contigs

  main:
    contigs_split = PreProcessContigs(contigs) | flatten | map { path -> tuple(path.baseName, "${path}/contigs.fasta") }
    mga = Mga(contigs_split) | flatten | map { path -> tuple(path.parent.baseName, path) }
    contigs_split.join(mga) | GeneExtractor
    cds_nuc = GeneExtractor.out.cds_nuc | collectFile()
    cds_prot = GeneExtractor.out.cds_prot | collectFile()
    cds_split = GeneExtractor.out.cds_prot | flatten | map { path -> tuple(path.parent.baseName, path) }
    Priam(cds_split)
    cds_coarse_split = SplitCds(cds_prot) | flatten | map { path -> tuple(path.baseName, "${path}/contigs.fasta") }
    cds_coarse_split | Diamond
    cds_coarse_split | Interproscan
    Gff(cds_prot, Mga.out | collectFile(), Priam.out, Diamond.out, Interproscan.out)
    export_ch = cds_prot.mix(cds_nuc, Diamond.out, Interproscan.out, Gff.out) | collect //Priam is exported separately
    Export("functionalAssignment", export_ch)
}